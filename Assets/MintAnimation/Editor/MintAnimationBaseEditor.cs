using System.Collections.Generic;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEditor;
using UnityEngine;

namespace MintAnimation.Editor
{
    [CustomPropertyDrawer(typeof(MintPlayerBase), true)]
    public class MintAnimationBaseEditor : PropertyDrawer
    {

        public static Dictionary<string, Color> HandlerColors = new Dictionary<string, Color>()
                                                                {
                                                                    {"MintPlayerEuler", Color.magenta},
                                                                    {"MintPlayerGraphicColor", Color.blue},
                                                                    {"MintPlayerPosition", Color.cyan},
                                                                    {"MintPlayerRotation", Color.red},
                                                                    {"MintPlayerScale", Color.yellow},
                                                                    {"MintPlayerAnchoredPos", Color.green},
                                                                    {"MintPlayerGraphicAlpha", new Color(0.72f, 0f, 1f)},
                                                                    {"MintPlayerSizeDelta", new Color(1f, 0.4f, 0f)},
                                                                };

        protected float _propertyHeight;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var defaultHeight = 60;
            var titleRect     = new Rect(position.position, new Vector2(position.width, defaultHeight));
            if (HandlerColors.ContainsKey(property.type))
            {
                EditorGUI.DrawRect(titleRect, HandlerColors[property.type] * new Color(1, 1, 1, 0.15f));
            }
            else
            {
                EditorGUI.DrawRect(titleRect, Color.black * new Color(1, 1, 1, 0.15f));
            }

            this._propertyHeight = 0;
            this.DrawExtend(position, property);
            this._propertyHeight += defaultHeight;
            this.DrawBase(position, property);
        }

        protected void DrawBase(Rect position, SerializedProperty property)
        {
            var tweenOptions     = property.FindPropertyRelative("TweenOptions");
            var isLocal          = property.FindPropertyRelative("IsLocal");
            var isAutoStartValue = property.FindPropertyRelative("IsAutoStartValue");
            var width            = position.width;
            var labelWidth       = EditorGUIUtility.labelWidth;
            this._propertyHeight += 3;
            var isLocalRect = new Rect(position.position + new Vector2(width * 0.5f, this._propertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            var isAutoRect  = new Rect(position.position + new Vector2(0,            this._propertyHeight), new Vector2(width * 0.5f, EditorGUIUtility.singleLineHeight));
            this._propertyHeight += EditorGUIUtility.singleLineHeight + 5;

            var optionsRect = new Rect(position.position + new Vector2(0, this._propertyHeight), new Vector2(width, this._propertyHeight));
            this._propertyHeight += EditorGUI.GetPropertyHeight(tweenOptions);

            EditorGUI.PropertyField(optionsRect, tweenOptions);
            EditorGUIUtility.labelWidth = width * 0.25f;
            if (this.ShowAutoBool())
                EditorGUI.PropertyField(isAutoRect, isAutoStartValue);
            else
                isAutoStartValue.boolValue = false;
            if (this.ShowLocalBool())
                isLocal.boolValue = EditorGUI.Popup(isLocalRect, isLocal.boolValue ? 1 : 0, new string[] {"World", "Local"}) == 1;
            else
                isLocal.boolValue = false;
            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected virtual void DrawExtend(Rect position, SerializedProperty property)
        {
            var curHeight  = 5f;
            var width      = position.width;
            var labelWidth = EditorGUIUtility.labelWidth;
            var startRect  = new Rect(position.position + new Vector2(0, curHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            curHeight += startRect.height + 10;
            var endRect = new Rect(position.position + new Vector2(0, curHeight), new Vector2(width, EditorGUIUtility.singleLineHeight));
            EditorGUIUtility.labelWidth = width * 0.25f;
            var startValue = property.FindPropertyRelative("StartValue");
            var endValue   = property.FindPropertyRelative("EndValue");
            if (startValue != null)
            {
                EditorGUI.PropertyField(startRect, startValue);
            }

            if (endValue != null)
            {
                EditorGUI.PropertyField(endRect, endValue);
            }

            EditorGUIUtility.labelWidth = labelWidth;
        }

        protected virtual bool ShowLocalBool() { return true; }
        protected virtual bool ShowAutoBool()  { return true; }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return this._propertyHeight;
            // return 199;
        }

    }
}