using System.Collections.Generic;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEngine;

namespace MintAnimation.Runtime.Components
{
    [CreateAssetMenu( fileName = "MintAnimConfig" , menuName = "MintAnimConfig")]
    public class MintAnimConfig : ScriptableObject
    {
        public List<MintPlayerMix> Anims;
    }
}