using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/Scale")]
    public class MintAnimScale : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerScale MintPlayer;
        public override Core.MintAnim.MintPlayerBase     MintAnimation() { return this.MintPlayer; }
    }
}