using UnityEngine;

namespace MintAnimation.Runtime.Components.MintAnimation
{
    [AddComponentMenu("MintAnimation/Animation/SizeDelta")]
    public class MintAnimSizeDelta : MintAnimBase
    {
        public          Core.MintAnim.MintPlayerSizeDelta MintPlayer;
        public override Core.MintAnim.MintPlayerBase  MintAnimation() { return this.MintPlayer; }
    }
}