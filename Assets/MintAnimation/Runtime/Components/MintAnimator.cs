using System;
using System.Collections.Generic;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEngine;

namespace MintAnimation.Runtime.Components
{
    [AddComponentMenu("MintAnimation/Animator")]
    public class MintAnimator : MonoBehaviour , IMintAnim
    {

        public bool              IsAutoPlay;
        public bool              IsLoop;
        public MintAnimConfig    AnimConfig;
        public List<MintPlayerMix> Anims;

        private bool              _isInit;
        private List<MintPlayerMix> _anims;

        private int               _curIndex;
        private int               _curPlayingCount;
        private List<MintPlayerMix> _curPlayingAnims;

        public Action OnComplete;

        private void Init()
        {
            if (this._isInit == false)
            {
                this._anims = new List<MintPlayerMix>();
                this._anims.Clear();
                if (this.AnimConfig != null)
                {
                    this._anims.AddRange(this.AnimConfig.Anims);
                }
                else
                {
                    this._anims.AddRange(this.Anims);
                }

                this._curPlayingAnims = new List<MintPlayerMix>();
                foreach (var anim in this._anims)
                {
                    if (anim.InitAnim(this.transform))
                    {
                        anim.OnComplete = this.OnAnimEnd;
                    }
                    else
                    {
                        return;
                    }
                }

                this._isInit = true;
            }
        }

        private void OnEnable()
        {
            if (this.IsAutoPlay)
            {
                if (this._isInit)
                {
                    foreach (var anim in this._anims)
                    {
                        anim.UpdateData();
                    }
                }
                this.StartAnim();
            }
        }

        private void OnDestroy()
        {
            if (this._isInit)
            {
                foreach (var anim in this._anims)
                {
                    anim.Player.Tween.Kill();
                }
            }
        }

        private void OnDisable()
        {
            if (this._isInit)
            {
                this.PauseAnim();
            }
        }

        public void StartAnim()
        {
            this.Init();
            if (!this._isInit)
                return;
            this._curIndex        = 0;
            this._curPlayingCount = 0;
            this.NextAnim();
        }

        public void PauseAnim()
        {
            if (!this._isInit)
                return;
            foreach (var playingAnim in this._curPlayingAnims)
            {
                playingAnim.Player.Tween.Pause();
            }
        }

        public void ResumeAnim()
        {
            if (!this._isInit)
                return;
            foreach (var playingAnim in this._curPlayingAnims)
            {
                playingAnim.Player.Tween.Resume();
            }
        }

        public void StopAnim()
        {
            if (!this._isInit)
                return;
            foreach (var playingAnim in this._curPlayingAnims)
            {
                playingAnim.Player.Tween.Pause();
            }

            this._curIndex = this._anims.Count - 1;
            this._curPlayingAnims.Clear();
            this._curPlayingCount = 0;
            this.OnCompleteAnim();
        }

        private void OnAnimEnd()
        {
            this._curPlayingCount--;
            if (this._curPlayingCount == 0)
            {
                this._curPlayingAnims.Clear();
                this._curIndex++;
                if (this._curIndex >= this._anims.Count)
                {
                    if (this.IsLoop)
                    {
                        this.StartAnim();
                    }
                    else
                    {
                        this.OnCompleteAnim();
                    }
                }
                else
                {
                    this.NextAnim();
                }
            }
        }

        private void OnCompleteAnim()
        {
            this.OnComplete?.Invoke();
        }

        private void NextAnim()
        {
            // Debug.Log("播放节点： " + this._anims[this._curIndex].AnimType);
            this._curPlayingCount++;
            
            this._curPlayingAnims.Add(this._anims[this._curIndex]);
            this._anims[this._curIndex].Player.Tween.Start();
            if (this._anims[this._curIndex].IsSync)
            {
                this._curIndex++;
                if (this._curIndex >= this._anims.Count)
                {
                    this._curIndex--;
                }
                else
                {
                    this.NextAnim();
                }
            }
        }

    }
}