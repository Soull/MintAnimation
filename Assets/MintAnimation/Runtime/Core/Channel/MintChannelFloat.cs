using System;

namespace MintAnimation.Core
{
    public class MintChannelFloat : MintChannelBase<float>, IMintChannel
    {


        public MintChannelFloat(Action<float> onProcess, float startValue, float endValue) : base(onProcess, startValue, endValue) { }
        public void OnProcessUpdate(float     processValue) { this.OnProcess?.Invoke((this.EndValue - this.StartValue) * processValue + this.StartValue); }
        public void UpdateStartValue(object   startValue)   { this.StartValue = (float) startValue; }
        public void UpdateEndValue(object     endValue)     { this.StartValue = (float) endValue; }

    }
}