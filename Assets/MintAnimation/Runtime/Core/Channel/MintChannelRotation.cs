using System;
using UnityEngine;

namespace MintAnimation.Core
{
    public class MintChannelRotation : MintChannelBase<Quaternion>, IMintChannel
    {
        public MintChannelRotation(Action<Quaternion> onProcess, Quaternion startValue, Quaternion endValue) : base(onProcess, startValue, endValue) { }
        public void OnProcessUpdate(float          processValue) { this.OnProcess?.Invoke(Quaternion.Slerp(this.StartValue, this.EndValue, processValue)); }
        public void UpdateStartValue(object        startValue)   { this.StartValue = (Quaternion) startValue; }
        public void UpdateEndValue(object          endValue)     { this.StartValue = (Quaternion) endValue; }
    }
}