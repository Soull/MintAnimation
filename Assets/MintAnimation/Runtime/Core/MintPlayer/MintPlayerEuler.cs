using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerEuler: MintPlayerBase
    {
        public Vector3 StartValue;
        public Vector3 EndValue;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartV3;
            this.EndValue = playerMix.EndV3;
        }

        protected override IMintChannel Channel() { return new MintChannelVector3(this.OnUpdate, this.StartValue, this.EndValue); }

        private void OnUpdate(Vector3 obj)
        {
            if (this.IsLocal)
            {
                this._transform.localEulerAngles = obj;
            }
            else
            {
                this._transform.eulerAngles = obj;
            }
        }

        public override void OnAutoStartValue()
        {
            if (this.IsLocal)
            {
                this.StartValue = this._transform.localEulerAngles;
            }
            else
            {
                this.StartValue = this._transform.eulerAngles;
            }
        }

    }
}