using System;
using MintAnimation.Core;
using UnityEngine;
using UnityEngine.UI;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerGraphicAlpha : MintPlayerBase
    {
        public float StartValue = 0;
        public float EndValue   = 1;

        private CanvasGroup _graphic;

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartF;
            this.EndValue   = playerMix.EndF;
        }

        protected override IMintChannel Channel() { return new MintChannelFloat(this.OnUpdate, this.StartValue, this.EndValue); }

        public override bool Init(Transform transform)
        {
            this._graphic = transform.GetComponent<CanvasGroup>();
            if (this._graphic == null)
            {
                Debug.LogError("缺少关键组件： CanvasGroup");
                return false;
            }
            return base.Init(transform);
        }
        private void OnUpdate(float obj) { this._graphic.alpha = obj; }

        public override void OnAutoStartValue() { this.StartValue = this._graphic.alpha; }

    }
}