using System;
using MintAnimation.Core;
using UnityEngine;

namespace MintAnimation.Runtime.Core.MintAnim
{
    [Serializable]
    public class MintPlayerSizeDelta : MintPlayerBase
    {

        public Vector3 StartValue;
        public Vector3 EndValue;

        private RectTransform _rectTransform;


        public override bool Init(Transform transform)
        {
            this._rectTransform = (RectTransform) transform;
            if (this._rectTransform == null)
            {
                Debug.LogError("缺少关键组件： RectTransform");
                return false;
            }
            return base.Init(transform);
        }

        protected override void SetMixData(MintPlayerMix playerMix)
        {
            this.StartValue = playerMix.StartV3;
            this.EndValue   = playerMix.EndV3;
        }

        protected override IMintChannel Channel() { return new MintChannelVector3(this.OnUpdate, this.StartValue, this.EndValue); }

        public override void OnAutoStartValue() { this.StartValue = this._rectTransform.sizeDelta; }

        private void OnUpdate(Vector3 obj) { this._rectTransform.sizeDelta = obj; }

    }
}