namespace MintAnimation.Runtime.Core.MintAnim
{
    public enum MintPlayerType
    {
        None,
        Position,
        Rotation,
        Scale,
        Euler,
        GraphicColor,
        GraphicAlpha,
        SizeDelta,
        AnchoredPos
    }
}