/*
* Author: Foldcc
* Mail:   lhyuau@qq.com
*/

using System;
using MintAnimation.Core;
using MintAnimation.Runtime.Core.MintAnim;
using UnityEngine;
using UnityEngine.UI;

namespace MintAnimation.Tests.ReorderableListTest
{
    [Serializable]
    public class TestSingeItem : UnityEngine.MonoBehaviour
    {

        public MintPlayerScale Item;
        //
        private void Awake()
        {
            this.Item.Init(this.transform);
        }
        
        private void OnEnable()
        {
            this.Item.Tween.Start();
        }
        
        private void OnDisable()
        {
            this.Item.Tween.Pause();
        }

    }

}